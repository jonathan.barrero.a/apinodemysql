const mysql = require('mysql')
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'username',
    password: 'pass',
    database: 'db'
});

connection.connect(function(error){
    if(!!error){
        console.log(erro);
    }else{
        console.log('Conectado');
    }
})

module.exports = connection;